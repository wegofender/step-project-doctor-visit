import axios from 'axios'

export default class AJAX {

  static cardsUrl = 'http://cards.danit.com.ua/cards';
  static headers = {
    'Authorization': `Bearer ${localStorage.getItem('authToken')}`,
  };

  static send(cardProps) {
    return axios.post(AJAX.cardsUrl,  cardProps, {
      headers: AJAX.headers
    })
      .then(response => cardProps.id = response.data.id)
  }

  static getAll() {
    return axios.get(AJAX.cardsUrl, {
      headers: AJAX.headers
    })
  }

  static edit(id, data) {
    return axios.put(`${AJAX.cardsUrl}/${id}`, data, {
      headers: AJAX.headers
    })
  }

  static get(id) {
    return axios.get(`${AJAX.cardsUrl}/${id}`, {
      headers: AJAX.headers
    })
  }

  static delete(id) {
    return axios.delete(`${AJAX.cardsUrl}/${id}`, {
      headers: AJAX.headers
    })
  }
}
