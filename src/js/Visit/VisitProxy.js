import Therapist from './Therapist';
import Dentist from './Dentist';
import Cardiologist from './Cardiologist';

const VisitMap = {
  Therapist,
  Dentist,
  Cardiologist
};

class VisitProxy {
  constructor(type, options) {
    return options ? new VisitMap[type](options) : VisitMap[type]
  }
}

export {VisitProxy, VisitMap}