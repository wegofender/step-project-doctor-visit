import AJAX from '../AJAX/AJAX';

export default class Visit {

  static basicFields = {
    fullName: {
      description: 'ФИО:',
      name: 'fullName',
      type: 'text',
      required: true,
      value: ''
    },
    age: {
      description: 'Возраст, полных лет:',
      name: 'age',
      type: 'text',
      required: true,
      value: ''
    },
    purpose: {
      description: 'Цель визита:',
      name: 'purpose',
      type: 'text',
      required: true,
      value: ''
    }
  };

  static dragSource = null;

  constructor(props) {
    console.log(props);
    const {
      type,
      localType,
      id,
      data:
        {
          fullName,
          ...additionalProps
        }
    } = props;

    this.id = id;
    this.fullName = fullName;
    this.localType = localType;
    this.additionalProps = additionalProps;
    this.commentary = props.data.commentary;

    this.showDetails = this.showDetails.bind(this);
    this.hideDetails = this.hideDetails.bind(this);
    this.editClickHandler = this.editClickHandler.bind(this);
    this.removeCardHandler = this.removeCardHandler.bind(this);
    this.editVisit = this.editVisit.bind(this);

    this.container = document.getElementById('cards-container');
    this.card = null;
    this.info = null;
    this.infoContainer = null;

    this.editEvent = new CustomEvent('visit-card.edit', {
      bubbles: true,
      detail: {
        fields: {},
        editCallback: this.editVisit,
        id: this.id,
        type: type,
        localType: this.localType
      }
    })
  }

  addListeners() {
    const card = this.card;
    card.querySelector('#visit-card-edit').addEventListener('click', this.editClickHandler);
    card.querySelector('#visit-card-remove').addEventListener('click', this.removeCardHandler);
    card.querySelector('#visit-details-btn').addEventListener('click', () => {
      this.info ? this.hideDetails() : this.showDetails();
    });
  }

  addDragAndDrop() {
    this.card.addEventListener('dragstart', this.handleDragStart);
    this.card.addEventListener('dragenter', this.handleDragEnter);
    this.card.addEventListener('dragover', this.handleDragOver);
    this.card.addEventListener('dragleave', this.handleDragLeave);
    this.card.addEventListener('drop', this.handleDrop);
    this.card.addEventListener('dragend', this.handleDragEnd);
  }

  renderCard() {

    const visitCard = document.createElement('div');
    visitCard.classList.add('visit-card');
    visitCard.setAttribute('draggable', 'true');

    visitCard.innerHTML = this.renderCardContent();

    this.card = visitCard;
    this.container.append(this.card);

    this.checkContainer();
    this.addListeners();
    this.addDragAndDrop()
  }

  renderCardContent() {
    return (
      `
       <div class="visit-card__header">
              <div class="visit-card__buttons">
                <button class="btn btn--icon" id="visit-card-edit">
                  <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn--icon" id="visit-card-remove">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
          <div class="card__body">
          <div class="main-info">
            <p class="main-info__item main-info__item--name" id="fullName-field">
              ${this.fullName.value}
            </p>

            <p class="main-info__item main-info__item--type">
              ${this.localType}
            </p>
          </div>
          
          <div id="additional-info"></div>

          <button class="btn btn--load" id="visit-details-btn">
            Показать больше
          </button>
          </div>
       </div>
       `
    );
  }

  showDetails() {
    this.infoContainer = this.card.querySelector('#additional-info');
    this.info =
      `<div class="additional-info">
      ${Object
        .keys(this.additionalProps)
        .map(key => {
          return (
            `<p class="additional-info-item">
            <span class="additional-info-item__name" id="${key}-field">
              ${this.additionalProps[key].locale}
            </span>
            ${this.additionalProps[key].value}
            </p>
            `
          )
        })
        .join('')}
    </div>`;
    this.infoContainer.innerHTML = this.info;
    this.card.querySelector('#visit-details-btn').innerText = 'Скрыть';
  }

  hideDetails() {
    this.info = null;
    this.infoContainer.innerHTML = this.info;
    this.card.querySelector('#visit-details-btn').innerText = 'Показать больше';
  }

  editClickHandler(e) {
    const localValues = Object.assign({}, {fullName: this.fullName}, this.additionalProps);
    const localFields = Object.keys(this.constructor.fields).reduce((clonedFields, key) => {
      clonedFields[key] = {...this.constructor.fields[key]};
      return clonedFields
    }, {});

    for (let key in localFields) {
      localFields[key].value = localValues[key].value
    }

    this.editEvent.detail.fields = localFields;

    // пришлось передавать через ивент в global scope, не работает импорт модалки, два часа гуглил, все равно не
    // работает ))

    e.target.dispatchEvent(this.editEvent)
  }

  editVisit({fullName, type, localType, ...additionalProps}) {
    this.fullName = fullName;
    this.additionalProps = additionalProps;
    this.commentary = additionalProps.commentary;
    this.info = null;

    this.card.innerHTML = this.renderCardContent();
    this.addListeners()
  }

  async removeCardHandler() {
    this.card.remove();
    this.checkContainer();

    await AJAX.delete(this.id)
  }

  checkContainer() {
    if (this.container.children.length === 0) {
      this.container.classList.add('cards-container__empty');
    } else if (this.container.classList.contains('cards-container__empty')) {
      this.container.classList.remove('cards-container__empty')
    }
  }

  handleDragStart(e) {
    Visit.dragSource = e.target;
    e.target.style.opacity = '0.4';
    e.target.style.filter = 'blur(4px)';
    e.dataTransfer.effectAllowed = 'move';
  }

  handleDragOver(e) {
    e.preventDefault();

    e.dataTransfer.dropEffect = 'move';
  }

  handleDragEnter(e) {
    e.target !== Visit.dragSource && e.target.classList.contains('visit-card') && e.target.classList.add('over');
  }

  handleDragLeave(e) {
    e.relatedTarget.classList.contains('cards-container') && e.target.classList.remove('over')
  }

  handleDrop(e) {
    const dropElement = e.target.closest('.visit-card');
    if (dropElement) {
      !dropElement.previousElementSibling
        ? dropElement.before(Visit.dragSource)
        : dropElement.after(Visit.dragSource)
    }
  }

  handleDragEnd(e) {
    e.target.style.opacity = '1';
    e.target.style.filter = 'none';
    document.querySelectorAll('.over').forEach(elem => elem.classList.remove('over'))
  }



}

