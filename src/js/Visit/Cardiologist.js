import Visit from './Visit';

export default class Cardiologist extends Visit {
    static fields = {
        ...Visit.basicFields,

        weightIndex: {
            description: 'Индекс массы тела:',
            name: 'weightIndex',
            type: 'text',
            required: true,
            value: ''
        },
        bloodPressure: {
            description: 'Нормальное кровяное давление:',
            name: 'bloodPressure',
            type: 'text',
            required: true,
            value: ''
        },
        diseases: {
            description: 'Перенесенные сердечно сосудитые заболевания:',
            name: 'diseases',
            type: 'text',
            required: true,
            value: ''
        },

        commentary: {
            description: 'Комментарий:',
            name: 'commentary',
            type: 'textarea',
            required: false,
            value: ''
        }
    };

    static types = {
        type: 'Cardiologist',
        localType: 'Кардиолог'
    };


    constructor(props) {
        super(props);
    }
}