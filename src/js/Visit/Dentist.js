import Visit from './Visit';

export default class Dentist extends Visit {
    static fields = {
        ...Visit.basicFields,

        lastVisitDate: {
            description: 'Дата последнего визита:',
            name: 'lastVisitDate',
            type: 'text',
            required: true,
            value: ''
        },

        commentary: {
            description: 'Комментарий:',
            name: 'commentary',
            type: 'textarea',
            required: false,
            value: ''
        }
    };

    static types = {
        type: 'Dentist',
        localType: 'Стоматолог'
    };

    constructor(props) {
        super(props);
    }
}