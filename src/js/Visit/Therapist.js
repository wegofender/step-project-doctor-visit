import Visit from './Visit';

export default class Therapist extends Visit {
    static fields = {
        ...Visit.basicFields,

        commentary: {
            description: 'Комментарий:',
            name: 'commentary',
            type: 'textarea',
            required: false,
            value: ''
        }
    };

    static types = {
        type: 'Therapist',
        localType: 'Терапевт'
    };

    constructor(props) {
        super(props)
    }
}