export default class Auth {
    constructor({email, password}) {
        this.url = 'http://cards.danit.com.ua/login';
        this.email = email;
        this.password = password;
    }

    async setAuthToken() {
        const authProps = JSON.stringify({
            email: this.email,
            password: this.password
        });
        const response = await fetch(this.url, {
            method: 'POST',
            body: authProps
        });

        const result = await response.json();
        const token = await result.token;

        localStorage.setItem('authToken', await token)
    }
}