import '../scss/style.scss'
import Auth from './Auth/Auth';
import AJAX from './AJAX/AJAX';
import authProps from './Auth/auth-props';

import {VisitProxy} from './Visit/VisitProxy';
import CreateModal from './Modal/CreateModal';
import EditModal from './Modal/EditModal';

document.addEventListener('DOMContentLoaded', async () => {
  if (localStorage.getItem('token') ) {
    const auth = new Auth(authProps);
    await auth.setAuthToken();
  }

  // const ajax = new AJAX();
  AJAX.getAll()
    .then(res => {
      res.data.forEach(cardProps => {
        const visit = new VisitProxy(cardProps.type, cardProps);
        visit.renderCard()
      })
    })
})

CreateModal.openBtn.addEventListener('click', () => {
  let createModal = new CreateModal()
  createModal.showModal()
})

document.addEventListener('modal.submit', e => {
  const cardProps = e.detail;
  const cardType = cardProps.type;

  const visit = new VisitProxy(cardType, cardProps);
  visit.renderCard();
})



document.addEventListener('visit-card.edit', (e) => {
  const props = e.detail;
  const editModal = new EditModal(props)
  editModal.showModal();
})


