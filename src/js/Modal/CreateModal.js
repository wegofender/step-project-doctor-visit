import Modal from './Modal';
import axios from 'axios'

import {VisitMap} from '../Visit/VisitProxy';
import AJAX from '../AJAX/AJAX';

export default class CreateModal extends Modal {
  static openBtn = document.querySelector('#create-button');

  constructor() {
    super();
    this.visitSelect = this.renderSelect();
    this.form = this.renderForm();
    this.visitSelectChangeHandler = this.visitSelectChangeHandler.bind(this);

    this.formContainer = null;
    this.submitEvent = new CustomEvent('modal.submit', {
      bubbles: true,
      detail: {
        type: 'Therapist',
        localType: 'Терапевт',
        id: '',
        data: null
      }
    });

    // this.data = this.submitEvent.detail;
  }

  getFields(e) {
    const options = e.target.options;
    const currentOption = options[options.selectedIndex];
    const currentValue = currentOption.value;
    const currentOptionText = currentOption.text;

    this.fields = VisitMap[currentValue].fields;
    this.submitEvent.detail.type = currentValue;
    this.submitEvent.detail.localType = currentOptionText;
  }

  addListeners(modal) {
    super.addListeners(modal);
    this.visitSelect.addEventListener('change', this.visitSelectChangeHandler);
  }

  renderSelect() {
    return (
      `<select class="visit-select" name="visit-select" id="visit-select">
         ${Object.keys(VisitMap).map(key => {
        const types = VisitMap[key].types;
        return `
             <option value=${types.type}>
               ${types.localType}
             </option>
           `
      })}
       </select>
       `
    )
  }

  showModal() {
    const modal = document.createElement('div');
    modal.className = 'modal';
    modal.innerHTML = `
          <div class="backdrop">
           <div class="modal-content">
             <div class="modal-header">
               <button class="btn btn--icon" id="modal-close-btn">
                 <i class="fas fa-times"></i>
               </button>
             </div>

             <div class="modal-body">
               <h3 class="modal-heading">
                 Выбрать врача:
               </h3>
               ${this.visitSelect}
               <div id="form-container">
                ${this.form}
               </div>
             </div>
           </div>
          </div>
        `;

    this.visitSelect = modal.querySelector('#visit-select');
    this.formContainer = modal.querySelector('#form-container');
    super.showModal(modal);
    this.addListeners(modal)
  }

  visitSelectChangeHandler(e) {
    this.form = this.renderForm(e);
    this.formContainer.innerHTML = this.form;
    this.addListeners(this.modal)
  }

  async submitHandler(e) {
    e.preventDefault();

    this.disableSubmit();

    const formData = this.getFormData(e.target);
    const cardProps = this.submitEvent.detail;
    this.submitEvent.detail.data = formData;

    await AJAX.send(cardProps);

    e.target.dispatchEvent(this.submitEvent);
    this.modal.remove()
  }
}
