import Therapist from '../Visit/Therapist';

export default class Modal {
  constructor(fields) {
    this.container = document.getElementById('app');

    this.modal = null;
    this.submitBtn = null;

    this.closeHandler = this.closeHandler.bind(this);
    this.getFields = this.getFields.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
    this.renderFields = this.renderFields.bind(this);

    this.fields = fields || Therapist.fields;
  }

  addListeners(modal) {
    modal.querySelector('#modal-close-btn').addEventListener('click', this.closeHandler);
    modal.querySelector('.backdrop').addEventListener('click', this.closeHandler);
    modal.querySelector('#modal-form').addEventListener('submit', this.submitHandler);
  }

  showModal(modal) {
    this.modal = modal;
    this.submitBtn = this.modal.querySelector('#submit-btn')
    this.container.append(modal);
  }

  renderFields(e) {
    e && this.getFields(e);

    return Object.keys(this.fields).map(key => {
      return (
        ` <div class="input-wrapper">
                    <label class="visit__label" for="${key}">
                        ${this.fields[key].description}
                    </label>
                    ${this.fields[key].type !== 'textarea' 
                        ?
                        `<input
                          class="visit__input"
                          data-locale="${this.fields[key].description}"
                          name="${this.fields[key].name}"
                          id='${key}'
                          type="${this.fields[key].type}"
                          placeholder=""
                          value="${this.fields[key].value}"
                          required="${this.fields[key].required}"
                        >`
                        :
                        `<textarea
                          class="visit__textarea"
                          data-locale="${this.fields[key].description}"
                          name="${this.fields[key].name}"
                          id='${key}'
                          rows="3"
                          >${this.fields[key].value}</textarea>
                        `
                      }
                  </div>
                `)
    }).join('');
  }

  renderForm(e) {

    return `
          <form class="modal-form" id="modal-form">
            <div class="fields-container" id="fields-container">
              ${this.renderFields(e)}
            <button type="submit" class="btn btn--basic btn--create" id="submit-btn">
              Готово
            </button>
          </form>
          `;

  }

  closeHandler(e) {
    if (e.target.closest('#modal-close-btn')|| !e.target.closest('.modal-content')) {
      this.modal.remove()
    }
  }


  getFormData(form) {

    return [...form.elements].reduce((formData, input) => {
      input.type === 'text' || input.type === 'textarea'
        ? formData[input.name] =
          {
            locale: input.dataset.locale,
            value: input.value
          }
        : null;
      return formData
    }, {})
  }

  loader() {
    return `
     <div class='sk-double-bounce'>
       <div class='sk-child sk-double-bounce-1'></div>
       <div class='sk-child sk-double-bounce-2'></div>
     </div>
        `
  }

  disableSubmit() {
    this.submitBtn.innerHTML = this.loader();
    this.submitBtn.setAttribute('disabled', 'true');
  }
}