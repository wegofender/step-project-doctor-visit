import Modal from './Modal';
import AJAX from '../AJAX/AJAX';

export default class EditModal extends Modal {
    constructor({fields, editCallback, id, type, localType}) {
        super(fields);
        this.fields = fields;
        this.form = this.renderForm();
        this.submitHandler = this.submitHandler.bind(this);
        this.visitEdit = editCallback;
        this.id = id;
        this.type = type;
        this.localType = localType;
    }

    showModal() {
        const modal = document.createElement('div');
        modal.className = 'modal';
        modal.innerHTML = `
          <div class="backdrop">
           <div class="modal-content">
             <div class="modal-header">
               <button class="btn btn--icon" id="modal-close-btn">
                 <i class="fas fa-times"></i>
               </button>
             </div>

             <div class="modal-body">
               <h3 class="modal-heading">
                 Редактировать визит
               </h3>
               <div id="form-container">
                ${this.form}
               </div>
             </div>
           </div>
          </div>
        `;

        super.showModal(modal);
        this.addListeners(modal)
    }

    getFields() {
        return this.fields
    }

    async submitHandler(e) {
        e.preventDefault();
        this.disableSubmit();

        const formData = this.getFormData(e.target);
        const cardProps = {
            type: this.type,
            localType: this.localType,
            id: this.id,
            data: formData
        };


        await AJAX.edit(this.id, cardProps);

        this.modal.remove();
        this.visitEdit(formData);
    }
}