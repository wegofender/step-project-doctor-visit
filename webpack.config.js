const webpack = require('webpack');
const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');


module.exports = env => {

    return {
        context: path.resolve(__dirname, 'src'),
        entry: ['./js/index.js'],
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'bundle.js'
        },
        devServer: {
            contentBase: path.join(__dirname, 'dist'),
            compress: true,


            port: process.env.PORT || 8080
        },
        plugins: [
            new HTMLWebpackPlugin({
                template: './index.html',
                favicon: '../public/favicon.ico'
            }),
            new MiniCssExtractPlugin({
                filename: 'bundle.[hash].css'
            }),
            new CleanWebpackPlugin()
        ],
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    use: [
                        env.development
                          ? {loader: 'style-loader'}
                          : {
                              loader: MiniCssExtractPlugin.loader,
                              options: {
                                  publicPath: './static',
                                  minimize: true
                              }
                          },
                        {loader: 'css-loader'},
                        {loader: 'sass-loader'}
                    ]
                },
                {
                    test: /\.m?js$/,
                    use: 'babel-loader',
                    exclude: /node_modules/
                }
            ]
        }
    }
};

